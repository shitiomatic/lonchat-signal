module bitbucket.org/shitiomatic/lonchat/signal

go 1.12

require (
	github.com/gorilla/websocket v1.4.0
	github.com/prometheus/client_golang v1.0.0
	github.com/rs/zerolog v1.14.3
)
